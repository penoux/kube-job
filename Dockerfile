FROM alpine:3.20.3

ARG TARGETOS
ARG TARGETARCH

# renovate: datasource=github-releases depName=kubernetes-sigs/cluster-api VersionTemplate=v
ENV CLUSTERCTL_VERSION="1.6.4"
# renovate: datasource=github-tags depName=kubernetes/kubectl VersionTemplate=kubernetes-
ENV KUBECTL_VERSION="1.29.5"
# renovate: datasource=github-releases depName=sigstore/cosign VersionTemplate=v
ENV COSIGN_VERSION="2.2.4"

# hadolint ignore=DL3018
RUN apk update \
    && apk --no-cache add bash jq yq curl wget \
    && wget -q --show-progress --progress=bar:force https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/${TARGETOS}/${TARGETARCH}/kubectl -O /usr/local/bin/kubectl \
    && wget -q --show-progress --progress=bar:force https://github.com/kubernetes-sigs/cluster-api/releases/download/v${CLUSTERCTL_VERSION}/clusterctl-${TARGETOS}-${TARGETARCH} -O /usr/local/bin/clusterctl \
    && wget -q --show-progress --progress=bar:force https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign-${TARGETOS}-${TARGETARCH} -O /usr/local/bin/cosign \
    && chmod +x /usr/local/bin/kubectl /usr/local/bin/clusterctl /usr/local/bin/cosign \
    && kubectl version --client \
    && clusterctl version \
    && cosign version
